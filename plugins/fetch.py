# Part of XBlog Webmention support.
# Copyright (C) 2021 Alex Martin

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Module for fetching Web resources. Currently supported: HTTP(S), FTP(S), local filesystem, Gemini.
"""
from urllib import parse as urlparse
from ftplib import FTP, FTP_TLS
import ssl
from socket import getaddrinfo, socket, AF_INET6, SOCK_STREAM
from os import strerror
from time import sleep
import re
from mimetypes import guess_type
from requests import get

text_type_patterns = [re.compile("text/.*"), re.compile("application/(?:.+\\+)xml"), re.compile("application/(?:.+\\+)json")]

def fetch_http(uri):
    r = get(uri, timeout=5)
    r.raise_for_status()
    content_type = r.headers.get("Content-Type") or "application/octet-stream"

    for pattern in text_type_patterns: # Tack on guessed encoding if not explicitly specified.
        if pattern.match(content_type):
            if not "charset=" in content_type.lower() and r.encoding is not None:
                content_type = content_type + "; charset=" + r.encoding
            break

    return ([(attrs["url"], rel) for rel, attrs in r.links.items()], r.content, content_type)

def fetch_ftp(uri):
    uri = urlparse.urlsplit(uri)
    try:
        conn = FTP_TLS(uri.hostname, timeout=5, context=ssl.create_default_context())
        conn.login()
        conn.prot_p()
    except:
        conn = FTP(uri.hostname, timeout=5)
        conn.login()

    blocks = []
    def save(block):
        blocks.append(block)
    conn.retrbinary("RETR " + uri.path, save) # FIXME: FTP command injection?
    conn.quit()
    return ([], b"".join(blocks), "application/octet-stream")

def fetch_file(uri):
    path = urlparse.urlsplit(uri).path
    with open(path, "rb") as f:
        return ([], f.read(), "application/octet-stream")

def connect_gemini(host, port):
    tls = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
    tls.check_hostname = False # Gemini doesn't require valid certificates.
    tls.verify_mode = ssl.CERT_NONE
    tls.minimum_version = ssl.TLSVersion.TLSv1_2

    infos = getaddrinfo(host, port, type=SOCK_STREAM)
    for af, sock_type, _, _, addr in infos:
        sock = tls.wrap_socket(socket(af, sock_type), server_hostname=host)
        sock.settimeout(5)
        err = sock.connect_ex(addr)
        if err == 0:
            break # Success!

    if err != 0:
        raise OSError(err, strerror(err)) # Delayed raise of last error only if all connection attempts fail.

    return sock

class GeminiException(Exception): pass

def fetch_gemini(uri, allow_retry=True, redirects_left=3):
    parsed = urlparse.urlsplit(uri)
    if parsed.scheme != "gemini":
        raise GeminiException("'" + parsed.scheme + "' is not Gemini")

    sock = connect_gemini(parsed.hostname, parsed.port or 1965) # Open TLS.

    sock.sendall(bytes(uri, encoding="UTF-8") + b"\r\n") # Send request.

    head_chunk = b"" # Buffer.
    head_split = [b""] # Initial state to make while loop happy.
    while len(head_split) == 1: # Read blocks until first CRLF found to mark end of header. When the separator is found, split()[1] will exist, even if it's b"".
        head_chunk += sock.recv(4096)
        head_split = head_chunk.split(b"\r\n", maxsplit=1)

    [head, first_data] = head_split # Break up bytes so far.
    [status, meta] = str(head, encoding="UTF-8").split(" ", maxsplit=1)

    if status[0] == "4" and allow_retry: # Retry once on temporary errors.
        if status == "44": # If asked to wait, do so within reason.
            try:
                secs = int(meta)
            except:
                raise GeminiException("Asked to wait '" + meta + "', not a valid number of seconds")

            if secs <= 5 and secs >= 0:
                sleep(secs)
            elif secs > 5:
                raise GeminiException("Asked to wait " + meta + " seconds, longer than maximum of 5")
            elif secs < 0:
                raise GeminiException("Asked to wait " + meta + " seconds, which would require time travel")

        return fetch_gemini(uri, allow_retry=False, redirects_left=redirects_left) # Retry.
    elif status[0] == "3" and redirects_left > 0: # Follow only the limit of redirects.
        return fetch_gemini(meta, redirects_left=redirects_left-1)
    elif status[0] == "2": # Success! Get data.
        data = first_data # This would be more elegant, but it's possible for first_data to be b"" but for there to be more data in the pipe, e.g. if the server sends just the header then waits a bit before sending the body.
        latest_data = sock.recv(4096)
        while latest_data != b"":
            data = data + latest_data
            latest_data = sock.recv(4096)
        data = data + latest_data
        return ([], data, meta)
    else: # Input, permanent error, certificate required, temporary error and no retry allowed, or too many redirects.
        raise GeminiException("Status " + status)

scheme_map = {
    "http": fetch_http,
    "https": fetch_http,
    "ftp": fetch_ftp,
    "ftps": fetch_ftp,
    "gemini": fetch_gemini,
    "file": fetch_file
}

def typeid_protocol(uri, fetch_result, kwargs):
    """
    Use the type returned by the protocol fetcher.
    """
    return fetch_result[2]

def typeid_caller(uri, fetch_result, kwargs):
    """
    Use the type specified in the predicted_media_type argument to get_links()
    """
    return kwargs["predicted_media_type"]

def typeid_mimetypes(uri, fetch_result, kwargs):
    """
    Use the type guessed by the standard library's mimetype module.
    """
    return guess_type(uri, strict=False)[0]

def typeid_magic(uri, fetch_result, kwargs):
    """
    Use the type guessed by libmagic, which is often quite accurate. The third-party magic module must be installed, so this isn't used by default.
    """
    import magic
    return magic.from_buffer(fetch_result[1], mime=True)

def fetch_uri(uri, typeid_order=[typeid_protocol, typeid_caller, typeid_mimetypes], **kwargs):
    """
    Fetch a URI and return a (links, content, mediatype) tuple. The links are a list of (uri, rel) tuples, the content is bytes, the media type is a string in the same format as MIME and HTTP's Content-Type header; it may include, e.g., a charset parameter.

    If you have a good guess as to what the media type of the result will be, pass it as the keyword argument predicted_media_type.

    This function tries very hard to return a useful media type. With protocols like HTTP and Gemini, this isn't a serious problem, since the server usually returns an accurate media type. For some protocols, however, the server doesn't send this information, and sometimes it might not have it; in both cases, the media type comes back as "application/octet-stream", which isn't useful.

    fetch_uri() tries a series of functions to identify the correct type. Functions are called in the order specified in typeid_order; the result of the first to return a value other than "application/octet-stream" or None is used.

    The default order is: The type returned by the protocol fetcher; the type given in the predicted_media_type keyword argument; then the guess made by the standard library's mimetypes module. If you install the third-party magic module, you can add typeid_magic to the list to get significantly better guesses, especially when there is no recognized filename extension.

    If none of these return something useful, "application/octet-stream" will be the returned media type.
    """
    scheme = urlparse.urlsplit(uri).scheme
    fetch_result = scheme_map[scheme](uri)

    if not kwargs.get("predicted_media_type"):
        kwargs["predicted_media_type"] = "application/octet-stream"

    media_type = "application/octet-stream"
    for typeid in typeid_order:
        if media_type == "application/octet-stream" or media_type is None:
            media_type = typeid(uri, fetch_result, kwargs)
        else:
            break

    if media_type is None:
        media_type = "application/octet-stream"

    return (fetch_result[0], fetch_result[1], media_type)

# A monument to me forgetting to check documentation; the half-written Link: parser shortstopped by discovering requests already does that.
#def parse_link_header(header):
    #if header is None:
        #return []
    #links = []
    #state = "start"
    #for c in header:
        #if state == "start":
            #if c.isspace():
                #continue
            #if c != "<":
                #return links # Bail
            #state = "uri"
            #this_uri = ""
            #this_rel = ""
            #continue
        #elif state == "uri"
            #if c.isspace():
                #return links # Bail
            #if c != ">":
                #this_uri = this_uri + c # FI-XME: Inefficient
                #continue
            #state = "next"
            #continue
        #elif state == "next":
            #if c == ",":
                #links.append((this_rel, this_uri))
                #state = "start"
                #continue
            #if c == ";":
                #attr_name = ""
                #state = "attrstart"
                #continue
        #elif state == "attrstart":
            #if c.isspace():
                #continue
            #state = "attr"
            #continue
        #elif state == "attr":
            #if c.isspace():
                #links.append((, this_uri)) # Bail
                #return links
            #rel
