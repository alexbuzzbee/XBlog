# Part of XBlog Webmention support.
# Copyright (C) 2021 Alex Martin

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Module for finding links (URI references) in Web resources specified by URI. Uses fetch and contentlinks.

The http, https, and gemini schemes work much better because they usually return a useful media type; ftp, ftps, and file always return application/octet-stream, which means somebody has to guess the content type, which often works poorly.
"""
import re
from io import BytesIO, StringIO
from lxml import etree, html
from json import loads
from fetch import fetch_uri, typeid_protocol, typeid_caller, typeid_mimetypes, typeid_magic
import contentlinks

XML_ENCODING_DECL_RE = re.compile("<\\?xml\\s.*\\bencoding=.+\\?>")

def make_lxml_happy(data, text):
    if text and not XML_ENCODING_DECL_RE.match(text.splitlines()[0]): # lxml does not like XML documents given as strings if they have encoding declarations.
        return StringIO(text)
    else:
        return BytesIO(data)

def xml_finder(buf, base=None, schemes=None, rels=None, encoding=None):
    if encoding:
        try:
            text = str(buf, encoding=encoding)
        except:
            text = None
    return contentlinks.find_xml_links(etree.parse(make_lxml_happy(buf, text)), base, schemes, rels)

def html_finder(buf, base=None, schemes=None, rels=None, encoding=None):
    if encoding:
        try:
            text = str(buf, encoding=encoding)
        except:
            text = None
    return contentlinks.find_html_links(html.parse(make_lxml_happy(buf, text)), base, schemes, rels)

def json_finder(buf, base=None, schemes=None, rels=None, encoding="UTF-8"):
    return contentlinks.find_json_links(loads(buf, encoding=encoding), base, schemes, rels)

def gemtext_finder(buf, base=None, schemes=None, rels=None, encoding="UTF-8"):
    return contentlinks.find_gemtext_links(str(buf, encoding=encoding), base, schemes, rels)

def plaintext_finder(buf, base=None, schemes=None, rels=None, encoding="UTF-8"):
    return contentlinks.find_plaintext_links(str(buf, encoding=encoding), base, schemes, rels)

pattern_finders = {
    re.compile("application/(?:.+\\+)xml"): xml_finder,
    re.compile("application/(?:.+\\+)json"): json_finder
}

literal_finders = {
    "text/xml": xml_finder,
    "text/html": html_finder,
    "text/gemini": gemtext_finder,
    "text/plain": plaintext_finder
}

all_finders = [xml_finder, html_finder, json_finder, gemtext_finder, plaintext_finder]

def get_links(uri, schemes=None, rels=None, typeid_order=[typeid_protocol, typeid_caller, typeid_mimetypes], fallback_to_everything=False, **kwargs):
    """
    Get all the links (URI references) in the specified resource, including any links provided by protocol-specific means like HTTP's Link header. If schemes is given, only relative URI references and absolute ones in those schemes will be returned. Links are returned as (uri, rel) pairs; see the contentlinks module.

    The keyword arguments and typeid_order are passed to fetch_uri() to configure the process of determining the media type.

    If the fallback_to_everything argument is set to True, then, if the media type is "application/octet-stream", every link-finder will be tried. This is obviously slower than is desirable and might produce lower-quality results, but has a chance of working if nothing else does. If it is False (the default), only the links returned by the protocol are returned in this case (very often an empty list).

    If the media type is identified, but is not a supported type, only the links returned by the protocol are returned (very often an empty list).
    """
    fetch_result = fetch_uri(uri, typeid_order, **kwargs)
    links = contentlinks.postprocess_links(fetch_result[0], uri, schemes, rels)
    media_type = fetch_result[2]

    if media_type == "application/octet-stream":
        if fallback_to_everything:
            for finder in all_finders:
                links = links + finder(fetch_result[1], uri, schemes, rels)
            return links
        else:
            return links

    kwargs = {} # Use the **kwargs syntax in a rather weird way to let defaults take over for finder functions if we don't find a charset in the media type.
    parts = media_type.split(";") # Parse media type for charset.
    for part in parts[1:]:
        if part.strip().lower().startswith("charset="):
            kwargs["encoding"] = part.split("=", maxsplit=1)[1].split(maxsplit=1)[0].strip("\"'") # Mostly deal with various uncommon media type syntax including comments and quotes.
            break

    for name, finder in literal_finders.items():
        if name == parts[0].lower():
            return links + finder(fetch_result[1], uri, schemes, rels, **kwargs)

    for pattern, finder in pattern_finders.items():
        if pattern.match(parts[0].lower()):
            return links + finder(fetch_result[1], uri, schemes, rels, **kwargs)

    return links
