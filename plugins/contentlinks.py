# Part of XBlog Webmention support.
# Copyright (C) 2021 Alex Martin

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Module for finding links (URI references) in various kinds of Web resource content. Currently supported: XML, (X)HTML, JSON, Gemini gemtext, and plain text. Some of these are mostly guessing, others are very reliable; it depends on how consistent links are in the format in question.

The find_*_links() functions in this module return lists of (uri, rel) pairs, where uri is a string that at least looks like a URI reference and rel is a space-separated list of relationship values (they might be URIs, CUIREs, just words, or maybe other things), which is often empty. References are not necessarily valid; invalid URI references will be found, accepted, and returned by these functions. There may be more than one copy of a given reference returned.

In addition to their main input, the find_*_links() functions can also take a base URI, a list of URI schemes, and/or a list of rel values. If a base URI is given, all relative URI references are reinterpreted relative to that base URI, and absolute URIs are returned. If a list of schemes is given, absolute URI references will be returned only if they are in those schemes (if a base URI is given, relative references become absolute references in the scheme of the base URI and so are returned if the base URI's scheme is accepted; if no base URI is given, all relative references will be returned). If a list of rel values are given, URI references will be returned only if they have at least one of the specified rel values. Still no guarantees as to actual validity are made.
"""
import re
from urllib import parse as urlparse
from lxml import html, etree

URI_ISH_RE = re.compile("^[a-zA-Z][a-zA-Z0-9+-.]*:\S+$") # See RFCs 3986 sec. 3.1 and 2234 sec. 6.1

# Tables for finding links in XML documents. paths should have all places URI references can be found in XML formats defined by W3C, WHATWG, IETF, or otherwise generally-recognized standards for documents meant to be published on the Web. If there are any missing, this should be considered a bug.

# Standards currently believed to be covered: (X)HTML5, RDFa, XLink, XInclude, XForms.

# If anyone knows of a rel= value appropriate for form submission targets, that would close a significant hole.

paths = [
    ("//*[@href]", "./@href", "./@rel|./@property"),
    ("//*[@src]", "./@src", "./@rel|./@property"),
    ("//*[@resource]", "./@resource", "./@rel|./@property"),
    ("//html:object[@data]", "./@data", "./@rel|./@property"),
    ("//html:form[@action]", "./@action", "''"),
    ("//html:button[@formaction]", "./@formaction", "''"),
    ("//html:input[@formaction]", "./@formaction", "''"),
    ("//*[@xlink:href]", "./@xlink:href", "./@rel|./@property"),
    ("//xforms:submission[@action]", "./@action", "''"),
]

namespaces = {
    "html": "http://www.w3.org/1999/xhtml",
    "xlink": "http://www.w3.org/1999/xlink",
    #"xi": "http://www.w3.org/2001/XInclude",
    "xforms": "http://www.w3.org/2002/xforms"
}

def postprocess_links(links, base=None, schemes=None, rels=None):
    if base is not None:
        fixed_links = []
        for (link, rel) in links:
            fixed_links.append((urlparse.urljoin(base, link), rel))
        links = fixed_links

    if rels is not None:
        good_links = []
        for (link, rel) in links:
            for rel_word in rel.split():
                if rel_word in rels:
                    good_links.append((link, rel))
                    break # Stop checking this link's words, since we already know it's good.
        links = good_links

    if schemes is not None:
        good_links = []
        for (link, rel) in links:
            uri = urlparse.urlsplit(link) # Include relative URIs and URIs in an accepted scheme.
            if uri.scheme == "" or uri.scheme in schemes:
                good_links.append((link, rel))
        links = good_links

    return links

def find_xml_links(root, base=None, schemes=None, rels=None):
    """
    Find links in the given XML (including XHTML) root element (from lxml's etree module) and its children. Very reliable, but may miss links in attributes not commonly known to have URIs.
    """
    xpath = etree.XPathEvaluator(root, namespaces=namespaces)
    links = []
    for elem_path, href_path, rel_path in paths:
        for elem in xpath(elem_path): # Find elements to look at.
            rel = elem.xpath(rel_path, namespaces=namespaces)
            if isinstance(rel, list): # Might be more than one result (different rel-like attributes); if only one, it's returned as a string. Make it always be one string.
                rel = " ".join(rel)

            href = elem.xpath(href_path)
            if isinstance(href, list): # Might be more than one result (different href-like attributes); if only one, it's returned as a string. Return each as a separate link.
                for uri in href:
                    links.append((uri, rel))
            else:
                links.append((href, rel))

    return postprocess_links(links, base, schemes, rels)

def find_html_links(root, base=None, schemes=None, rels=None):
    """
    Find links in the given HTML root element (from lxml's html module) and its children (clobbering them by giving them an XML namespace). Works by converting HTML to XHTML and passing it to find_xml_links(); as reliable as find_xml_links() as long as the HTML parses okay.
    """
    html.html_to_xhtml(root) # Modifies root.
    return find_xml_links(root, base, schemes, rels)

def find_json_links(value, base=None, schemes=None, rels=None):
    """
    Find strings that look like absolute URI references in the given JSON value (already parsed by the standard library's json module, or something equivalent). Not very reliable, since there are many things that look like URI references and are not. Becomes much more reliable when schemes are given.

    rel values are always empty.
    """
    if isinstance(value, str): # Return strings that look like URIs.
        if URI_ISH_RE.match(value):
            return postprocess_links([(value, "")], base, schemes, rels)
        else:
            return []
    elif isinstance(value, list): # Try each value in a list.
        links = []
        for elem in value:
            links = links + find_json_links(elem) # Don't pass on schemes or rels, since the filtering will be done at the end anyway.
        return postprocess_links(links, base, schemes, rels)
    elif isinstance(value, dict): # Try only the values of an object.
        links = []
        for item in value.values():
            links = links + find_json_links(item) # Don't pass on schemes or rels, since the filtering will be done at the end anyway.
        return postprocess_links(links, base, schemes, rels)
    else: # Ignore numbers and any other things that might show up.
        return []

def find_gemtext_links(gemtext, base=None, schemes=None, rels=None):
    """
    Find link URIs in the given Gemini gemtext. Very reliable as long as the URIs are links, otherwise they won't be found at all. find_plaintext_links() can be used if non-link URIs need to be found.

    rel values are always empty.
    """
    links = []
    preformatted = False
    for line in gemtext.splitlines(False):
        if line.startswith("```"): # Don't parse inside preformatted blocks.
            preformatted = not preformatted
        if line.startswith("=>") and not preformatted:
            links.append((line.split()[1], ""))
    return postprocess_links(links, base, schemes, rels)

brackets = {
    "<": ">",
    "[": "]",
    "(": ")",
    "{": "}",
    "'": "'",
    '"': '"'
}

punctuation = ".,?!"

def find_plaintext_links(text, base=None, schemes=None, rels=None):
    """
    Find substrings that look like absolute URI references in the given text. Not very reliable, since there are many things that look like URI references and are not. Becomes much more reliable when schemes are given. Can handle URIs wrapped in <>, [], (), {}, "", or ''. Tries to deal with punctuation following URIs, which could mangle a small number of URIs.

    rel values are always empty.
    """
    links = []
    for word in text.split():
        other_bracket = brackets.get(word[0]) # Look for a bracket; if it's found, remove the other one from the end.
        if other_bracket is not None:
            word = word.lstrip(word[0]).rstrip(other_bracket + punctuation)
        if URI_ISH_RE.match(word):
            links.append((word.rstrip(punctuation), ""))
    return postprocess_links(links, base, schemes, rels)
