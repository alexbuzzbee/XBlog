<?xml version="1.0" encoding="UTF-8"?>
<!--
XBlog, blogging for XSite.
Copyright (C) 2021 Alex Martin

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="document-uri" />
  <xsl:template match="dataset">
    <section>
      <h1>Mentions</h1>
      <p>Other pages on the Web that have linked here and told us about it are linked below with brief summaries, so you can see what they have to say.</p>
      <form method="post">
        <xsl:attribute name="action">
          <xsl:value-of select="$conf-blog-mention-uri" />
        </xsl:attribute>
        <fieldset>
          <legend>Add your reply here with <a href="https://www.w3.org/TR/2017/REC-webmention-20170112/">Webmention</a> (you must link to this page in its canonical form)</legend>
          <input type="hidden" name="target">
            <xsl:attribute name="value">
              <xsl:value-of select="$document-uri" />
            </xsl:attribute>
          </input>
          <label for="source">Page linking here</label>
          <input type="text" name="source" />
          <input type="Submit" value="Send" />
        </fieldset>
      </form>
      <ul>
        <xsl:apply-templates />
      </ul>
    </section>
  </xsl:template>
  <xsl:template match="record">
    <xsl:if test="@target=$document-uri">
      <li>
        <article>
          <h1><a><xsl:attribute name="href"><xsl:value-of select="@source" /></xsl:attribute><xsl:value-of select="@title" /></a></h1>
          <xsl:if test="@author!=''">
            <p><i>By <xsl:value-of select="@author" /></i></p>
          </xsl:if>
          <blockquote>
            <xsl:value-of select="@summary" />
          </blockquote>
        </article>
      </li>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
